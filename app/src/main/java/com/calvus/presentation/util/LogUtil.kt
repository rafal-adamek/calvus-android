package com.calvus.presentation.util

import android.util.Log
import com.calvus.BuildConfig

object LogUtil {
    fun d (tag: String = "", message: String = "") {
        if(BuildConfig.DEBUG) Log.d(tag, message)
    }

    fun i (tag: String = "", message: String = "") {
        Log.i(tag, message)
    }

    fun w (tag: String = "", message: String = "") {
        Log.w(tag, message)
    }

    fun e (tag: String = "", message: String = "") {
        Log.e(tag, message)
    }

    fun v (tag: String = "", message: String = "") {
        Log.v(tag, message)
    }

    fun wtf (tag: String = "", message: String = "") {
        Log.wtf(tag, message)
    }
}