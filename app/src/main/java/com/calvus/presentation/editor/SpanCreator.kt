package com.calvus.presentation.editor

import android.text.ParcelableSpan
import java.util.regex.Matcher

interface SpanCreator {
    fun create(m: Matcher): ParcelableSpan
}