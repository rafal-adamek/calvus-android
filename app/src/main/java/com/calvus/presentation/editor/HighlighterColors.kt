package com.calvus.presentation.editor

interface HighlighterColors {

    val headerColor: Int

    val linkColor: Int

    val listColor: Int

    val quotationColor: Int
}
