package com.calvus.presentation.editor

import android.graphics.Typeface
import android.text.Editable
import android.text.ParcelableSpan
import android.text.Spannable
import android.text.style.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class Highlighter(private val colors: HighlighterColors, val fontType: String, val fontSize: Int) {

    fun run(e: Editable): Editable {
        try {
            clearSpans(e)

            if (e.isEmpty()) {
                return e
            }

            createHeaderSpanForMatches(e, HighlighterPattern.HEADER, colors.headerColor)
            createColorSpanForMatches(e, HighlighterPattern.LINK, colors.linkColor)
            createColorSpanForMatches(e, HighlighterPattern.LIST, colors.listColor)
            createStyleSpanForMatches(e, HighlighterPattern.BOLD, Typeface.BOLD)
            createStyleSpanForMatches(e, HighlighterPattern.ITALICS, Typeface.ITALIC)
            createColorSpanForMatches(e, HighlighterPattern.QUOTATION, colors.quotationColor)
            createSpanWithStrikeThroughForMatches(e, HighlighterPattern.STRIKETHROUGH)
            createMonospaceSpanForMatches(e, HighlighterPattern.MONOSPACED)

        } catch (ex: Exception) {
            // Ignoring errors
        }

        return e
    }

    private fun createHeaderSpanForMatches(e: Editable, pattern: HighlighterPattern, headerColor: Int) {

        createSpanForMatches(e, pattern, HeaderSpanCreator(this, e, headerColor))
    }

    private fun createMonospaceSpanForMatches(e: Editable, pattern: HighlighterPattern) {

        createSpanForMatches(e, pattern, object : SpanCreator {
            override fun create(m: Matcher): ParcelableSpan {
                return TypefaceSpan("monospace")
            }
        })

    }

    private fun createSpanWithStrikeThroughForMatches(e: Editable, pattern: HighlighterPattern) {

        createSpanForMatches(e, pattern, object : SpanCreator {
            override fun create(m: Matcher): ParcelableSpan {
                return StrikethroughSpan()
            }
        })
    }

    private fun createStyleSpanForMatches(e: Editable, pattern: HighlighterPattern,
                                          style: Int) {
        createSpanForMatches(e, pattern, object : SpanCreator {
            override fun create(m: Matcher): ParcelableSpan {
                return StyleSpan(style)
            }
        })
    }

    private fun createColorSpanForMatches(e: Editable, pattern: HighlighterPattern,
                                          color: Int) {
        createSpanForMatches(e, pattern, object : SpanCreator {
            override fun create(m: Matcher): ParcelableSpan {
                return ForegroundColorSpan(color)
            }
        })
    }

    private fun createSpanForMatches(e: Editable, pattern: HighlighterPattern,
                                     creator: SpanCreator) {
        createSpanForMatches(e, pattern.pattern, creator)
    }

    private fun createSpanForMatches(e: Editable, pattern: Pattern,
                                     creator: SpanCreator) {
        val m = pattern.matcher(e)
        while (m.find()) {
            e.setSpan(creator.create(m), m.start(), m.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }

    private fun clearSpans(e: Editable) {

        clearSpanType(e, TextAppearanceSpan::class.java)
        clearSpanType(e, ForegroundColorSpan::class.java)
        clearSpanType(e, BackgroundColorSpan::class.java)
        clearSpanType(e, StrikethroughSpan::class.java)
        clearSpanType(e, StyleSpan::class.java)
    }

    private fun <T : CharacterStyle> clearSpanType(e: Editable, spanType: Class<T>) {

        val spans = e.getSpans(0, e.length, spanType)

        var n = spans.size
        while (n-- > 0) {
            e.removeSpan(spans[n])
        }
    }

}
