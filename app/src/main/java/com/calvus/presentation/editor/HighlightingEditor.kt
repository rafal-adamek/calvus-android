package com.calvus.presentation.editor

import android.content.Context
import android.os.Handler
import android.support.v7.widget.AppCompatEditText
import android.text.Editable
import android.text.InputFilter
import android.text.Spanned
import android.text.TextWatcher
import android.util.AttributeSet

class HighlightingEditor : AppCompatEditText {

    private var highlighter: Highlighter? = null

    internal interface OnTextChangedListener {
        fun onTextChanged(text: String)
    }

    private val onTextChangedListener: OnTextChangedListener? = null

    private val updateHandler = Handler()
    private val updateRunnable = Runnable {
        val e = text

        onTextChangedListener?.onTextChanged(e.toString())

        highlightWithoutChange(e)
    }
    private var modified = true

    constructor(context: Context) : super(context) {
        //if (AppSettings.get().isHighlightingEnabled()) {
            init()
        //}
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
//        if (AppSettings.get().isHighlightingEnabled()) {
            init()
//        }
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
//        if (AppSettings.get().isHighlightingEnabled()) {
            init()
//        }
    }

    private fun init() {

        filters = arrayOf<InputFilter>(IndentationFilter())

        val highlightingDelay = highlightingDelayFromPrefs

        highlighter = Highlighter(HighlighterColorsNeutral(),
                "sans-serif-regular",
                18)

        addTextChangedListener(
                object : TextWatcher {
                    override fun onTextChanged(
                            s: CharSequence,
                            start: Int,
                            before: Int,
                            count: Int) {
                    }

                    override fun beforeTextChanged(
                            s: CharSequence,
                            start: Int,
                            count: Int,
                            after: Int) {
                    }

                    override fun afterTextChanged(e: Editable) {
                        cancelUpdate()

                        if (!modified)
                            return

                        updateHandler.postDelayed(
                                updateRunnable,
                                highlightingDelay.toLong())
                    }
                })
    }

    private fun cancelUpdate() {
        updateHandler.removeCallbacks(updateRunnable)
    }

    private fun highlightWithoutChange(e: Editable) {
        modified = false
        highlighter!!.run(e)
        modified = true
    }

    private fun getStringFromStringTable(preference_key: Int): String {
        return this.context.getString(preference_key)
    }

    private val highlightingDelayFromPrefs: Int
        get() = 70

    private inner class IndentationFilter : InputFilter {
        override fun filter(
                source: CharSequence,
                start: Int,
                end: Int,
                dest: Spanned,
                dstart: Int,
                dend: Int): CharSequence {

            if (modified &&
                    end - start == 1 &&
                    start < source.length &&
                    dstart <= dest.length) {
                val newChar = source[start]

                if (newChar == '\n') {
                    return autoIndent(
                            source,
                            dest,
                            dstart,
                            dend)
                }
            }

            return source
        }

        private fun autoIndent(source: CharSequence, dest: Spanned, dstart: Int, dend: Int): CharSequence {

            val istart = findLineBreakPosition(dest, dstart)

            // append white space of previous line and new indent
            return source.toString() + createIndentForNextLine(dest, dend, istart)
        }

        private fun findLineBreakPosition(dest: Spanned, dstart: Int): Int {
            var istart = dstart - 1

            while (istart > -1) {
                val c = dest[istart]

                if (c == '\n')
                    break
                --istart
            }
            return istart
        }

        private fun createIndentForNextLine(dest: Spanned, dend: Int, istart: Int): String {
            var istart = istart
            //TODO: Auto-populate the next number for ordered-lists in addition to bullet points
            //TODO: Replace this
            if (istart > -1 && istart < dest.length - 1) {
                var iend: Int = ++istart

                while (iend < dest.length - 1) {
                    val c = dest[iend]

                    if (c != ' ' && c != '\t') {
                        break
                    }
                    ++iend
                }

                return if (iend < dest.length - 1) {
                    if (dest[iend + 1] == ' ') {
                        dest.subSequence(istart, iend).toString() + addBulletPointIfNeeded(dest[iend])
                    } else {
                        ""
                    }
                } else {
                    ""
                }
            } else return if (istart > -1) {
                ""
            } else if (dest.length > 1) { // You need at least a list marker and a space to trigger auto-list-item
                if (dest[1] == ' ') {
                    addBulletPointIfNeeded(dest[0])
                } else {
                    ""
                }
            } else {
                ""
            }
        }

        private fun addBulletPointIfNeeded(character: Char): String {
            return if (character == '*' || character == '+' || character == '-') {
                Character.toString(character) + " "
            } else {
                ""
            }

        }
    }

}