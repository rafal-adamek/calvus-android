package com.calvus.presentation.editor

class HighlighterColorsNeutral : HighlighterColors {

    override val headerColor = 0xffef6C00.toInt()
    override val linkColor = 0xff1ea3fd.toInt()
    override val listColor = 0xffdaa520.toInt()
    override val quotationColor = 0xff88b04b.toInt()

}
