package com.calvus.presentation

import android.databinding.BaseObservable
import com.calvus.domain.ReactiveManager
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel<out V : BaseContract.View> (
        protected val view : V,
        protected val reactiveManager: ReactiveManager) : BaseObservable(), BaseContract.BaseViewModel {

    protected val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onDestroy() {
        this.compositeDisposable.dispose()
    }
}