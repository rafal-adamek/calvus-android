package com.calvus.presentation.main

import com.calvus.data.models.Note
import com.calvus.data.models.Notepad
import com.calvus.presentation.BaseContract

class MainContract {

    interface View : BaseContract.View {
        fun openLoginScreen()
        fun openNotepad(notepadId: Int)
        fun openNote(noteId: Int)
        fun initNotepadMenu()
        fun initMain()
        fun addToNotepadMenu(notepad: Notepad)
        fun showProgress(isShowing: Boolean)
        fun showNotepadProgress(isShowing: Boolean)
    }

    interface ViewModel : BaseContract.ActivityView {
        fun validateToken()
        fun fetchNotepads()
        fun onNotepadSelected(notepadId: Int)
        fun onNoteSelected(note: Note)
        fun createNotepad(name: String)
    }
}