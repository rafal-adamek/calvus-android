package com.calvus.presentation.main

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.Menu
import android.view.MenuItem
import com.calvus.BR
import com.calvus.R
import com.calvus.data.models.Note
import com.calvus.data.models.Notepad
import com.calvus.databinding.ActivityMainBinding
import com.calvus.domain.injection.MainActivityModule
import com.calvus.presentation.BaseActivity
import com.calvus.presentation.BaseContract
import com.calvus.presentation.login.LoginActivity
import com.calvus.presentation.main.MainContract.View
import com.calvus.presentation.note.NoteFragment
import com.calvus.presentation.notepad.NotepadContract.InteractionListener
import com.calvus.presentation.notepad.NotepadFragment

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(),
        OnNavigationItemSelectedListener, View, InteractionListener {

    override val layoutId: Int get() = R.layout.activity_main
    override val containerId: Int = R.id.cl_container_main
    override val bindingId: Int = BR.mainViewModel

    private val component by lazy { app.component.plus(MainActivityModule(this)) }

    //Lifecycle

    override fun onCreate(savedInstanceState: Bundle?) {
        component.inject(this)
        super.onCreate(savedInstanceState)
        val toolbar = root.iAppBar.toolbar
        setSupportActionBar(toolbar)

        val drawer = root.drawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        val navigationView = root.navView
        navigationView.setNavigationItemSelectedListener(this)

        viewModel.validateToken()

        this.replaceFragment(fragment = NotepadFragment(),
                tag = NotepadFragment.TAG, animationType = BaseContract.ANIM_TYPE_NONE, addToBackStack = false)
    }

    //Implementation

    override fun initNotepadMenu() {
        val menu = root.navView.menu
        menu.setGroupCheckable(R.id.group_notepads, true, true)
        menu.add(R.id.group_notepads, 0, 1, "Add new notebook").setIcon(R.drawable.ic_add_box).isCheckable = false
    }

    override fun initMain() {
        this.viewModel.fetchNotepads()
    }

    override fun addToNotepadMenu(notepad: Notepad) {
        root.navView.menu.add(R.id.group_notepads, notepad.id, 1, notepad.name).setIcon(R.drawable.ic_notepad)
                .isCheckable = true
    }

    override fun onBackPressed() {
        val drawer = root.drawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation root item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {

            }
            else -> {
                this.viewModel.onNotepadSelected(item.itemId)
            }
        }

        root.drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun openLoginScreen() {
        val loginActivity = Intent(this, LoginActivity::class.java)
        startActivity(loginActivity)
        finish()
    }

    //TODO: Add logic for retrieving and refreshing fragments
    // fragment = this.getFragment(NotepadFragment.TAG) ?: NotepadFragment.create(notepadId),

    override fun openNotepad(notepadId: Int) = this.replaceFragment(
            fragment = this.getFragment(NotepadFragment.TAG) ?: NotepadFragment.create(notepadId),
            addToBackStack = true,
            tag = NotepadFragment.TAG,
            containerId = containerId
    )

    override fun onNoteClicked(note: Note) {
        this.viewModel.onNoteSelected(note)
    }

    override fun openNote(noteId: Int) = this.replaceFragment(
            fragment = NoteFragment.create(noteId),
            addToBackStack = true,
            tag = NoteFragment.TAG,
            containerId = containerId
    )

    //TODO: Add loading indicator
    override fun showProgress(isShowing: Boolean) {
        //this.root.iAppBar.contentMain.srlMain.isRefreshing = isShowing
    }

    override fun showNotepadProgress(isShowing: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
