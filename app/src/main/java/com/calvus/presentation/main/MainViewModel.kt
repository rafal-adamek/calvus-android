package com.calvus.presentation.main

import com.calvus.data.UserDataManager
import com.calvus.data.NotesRepository
import com.calvus.data.models.Note
import com.calvus.data.models.Notepad
import com.calvus.domain.ReactiveManager
import com.calvus.presentation.BaseViewModel
import com.calvus.presentation.main.MainContract.ViewModel
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class MainViewModel @Inject constructor(
        view: MainContract.View,
        private val userDataManager: UserDataManager,
        private val notesRepository: NotesRepository,
        reactiveManager: ReactiveManager
) : BaseViewModel<MainContract.View>(view, reactiveManager), ViewModel {

    //Implementation

    override fun validateToken() {
        if (userDataManager.retrieveAuthToken() == UserDataManager.TOKEN_DEFAULT) {
            view.openLoginScreen()
        } else {
            view.initMain()
        }
    }

    override fun fetchNotepads() {
        this.view.showProgress(true)
        notesRepository.getNotepads()
                .observeOn(reactiveManager.schedulerAndroidMain)
                .subscribeOn(reactiveManager.schedulerIo)
                .subscribe(
                        { notepad: Notepad ->
                            view.addToNotepadMenu(notepad)
                        },
                        { throwable: Throwable? ->
                            handleError(throwable)
                        },
                        {
                            view.showProgress(false)
                        },
                        { disposable: Disposable ->
                            view.initNotepadMenu()
                            compositeDisposable.add(disposable)
                        }
                )
    }

    override fun createNotepad(name: String) {
        this.view.showNotepadProgress(true)
        //TODO: Finish notepad creation
        notesRepository.createNotepad(name).subscribe()
    }

    override fun onNotepadSelected(notepadId: Int) {
        this.view.openNotepad(notepadId)
    }

    override fun onNoteSelected(note: Note) {
        this.view.openNote(note.id)
    }

    //TODO: Better error handling
    private fun handleError(throwable: Throwable?) {

    }
}