package com.calvus.presentation

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.calvus.App
import javax.inject.Inject

abstract class BaseFragment<V : ViewDataBinding, M : BaseViewModel<*>> : Fragment(), BaseContract.FragmentView {

    protected lateinit var root: V

    @Inject protected lateinit var viewModel: M

    @get:LayoutRes protected abstract val layoutId: Int

    protected abstract val bindingId: Int

    val app: App by lazy { activity.application as App }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        root = DataBindingUtil.inflate(inflater!!, layoutId, container, false)
        return root.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        root.setVariable(bindingId, viewModel)
    }

    override fun onDestroy() {
        this.root.unbind()
        this.viewModel.onDestroy()
        super.onDestroy()
    }
}