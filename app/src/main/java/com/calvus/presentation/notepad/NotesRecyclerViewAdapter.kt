package com.calvus.presentation.notepad

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.calvus.BR
import com.calvus.R
import com.calvus.data.models.Note

class NotesRecyclerViewAdapter(
        private val values: List<Note>,
        private val listener: NotepadContract.InteractionListener?)
    : RecyclerView.Adapter<NotesRecyclerViewAdapter.NoteHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_note, parent, false)
        return NoteHolder(view)
    }

    override fun onBindViewHolder(holder: NoteHolder, position: Int) {
        val note = values[position]

        holder.binding.setVariable(BR.note, note)
        holder.binding.executePendingBindings()

        holder.view.setOnClickListener { listener?.onNoteClicked(note) }
    }

    override fun getItemCount(): Int = values.size

    inner class NoteHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val binding: ViewDataBinding = DataBindingUtil.bind<ViewDataBinding>(view)
    }

}
