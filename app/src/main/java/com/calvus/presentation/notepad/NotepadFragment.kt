package com.calvus.presentation.notepad

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.calvus.BR
import com.calvus.R
import com.calvus.databinding.FragmentNotepadBinding
import com.calvus.domain.injection.NotepadFragmentModule
import com.calvus.presentation.BaseFragment
import com.calvus.presentation.notepad.NotepadContract.InteractionListener

/**
 * A fragment representing a list of Items.
 *
 *
 * Activities containing this fragment MUST implement the [InteractionListener]
 * interface.
 */
/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
class NotepadFragment : BaseFragment<FragmentNotepadBinding, NotepadViewModel>(), NotepadContract.View {

    override val layoutId: Int = R.layout.fragment_notepad
    override val bindingId: Int = BR.notepadViewModel

    private var parent: NotepadContract.InteractionListener? = null

    private val notesItemTouchCallback = object: ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
        override fun onMove(
                recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, target: RecyclerView.ViewHolder?
        ): Boolean = false

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {
            //Remove element
        }
    }

    private val notesAdapter by lazy {
        NotesRecyclerViewAdapter(this.viewModel.provideNoteList(), parent)
    }

    //Lifecycle

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is InteractionListener) {
            parent = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement InteractionListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        app.component.plus(NotepadFragmentModule(this)).inject(this)
        viewModel.setNotepadId(arguments?.getInt(NOTEPAD_ID_KEY) ?: NotepadViewModel.DEFAULT_NOTEPAD)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        root.rvNotes.adapter = notesAdapter

        val touchHelper = ItemTouchHelper(notesItemTouchCallback)
        touchHelper.attachToRecyclerView(root.rvNotes)

        return view
    }

    override fun onDetach() {
        super.onDetach()
        parent = null
    }

    //Implementation

    override fun showProgress(isShowing: Boolean) {
        root.srlNotes.isRefreshing = isShowing
    }

    override fun changeNotepad(notepadId: Int) {
        arguments.putInt(NOTEPAD_ID_KEY, notepadId)
        viewModel.setNotepadId(notepadId)
    }

    override fun notifyNoteAdded(position: Int) {
        this.notesAdapter.notifyItemInserted(position)
    }

    override fun init() {
        this.viewModel.refreshItems()
    }

    //Static

    companion object {
        val TAG: String = NotepadFragment::class.toString()
        private val NOTEPAD_ID_KEY = "NOTEPAD_ID_KEY"

        fun create(notepadId: Int): NotepadFragment {
            val fragment = NotepadFragment()
            val bundle = Bundle()
            bundle.putInt(NOTEPAD_ID_KEY, notepadId)
            fragment.arguments = bundle
            return fragment
        }
    }

}
