package com.calvus.presentation.notepad

import com.calvus.data.NotesRepository
import com.calvus.data.UserDataManager
import com.calvus.data.models.Note
import com.calvus.domain.ReactiveManager
import com.calvus.presentation.BaseViewModel
import com.calvus.presentation.notepad.NotepadContract.View
import com.calvus.presentation.notepad.NotepadContract.ViewModel
import io.reactivex.disposables.Disposable
import javax.inject.Inject

//TODO: Provide a dependency injection for network communication and fetching data from DB
//TODO: Retrofit 2 offline cache
//TODO: Retrofit 2 offline requests that are made when connected to the internet

class NotepadViewModel
@Inject constructor(
        view: View,
        private val notesRepository: NotesRepository,
        private val userDataManager: UserDataManager,
        reactiveManager: ReactiveManager
) : BaseViewModel<View>(view, reactiveManager), ViewModel {

    private var notepadId: Int = NotepadViewModel.DEFAULT_NOTEPAD
    private val notes = mutableListOf<Note>()

    override fun setNotepadId(notepadId: Int) {
        if(notepadId == NotepadViewModel.DEFAULT_NOTEPAD) {
            this.notepadId = userDataManager.retrieveLastNotepad()
        } else {
            this.notepadId = notepadId
            userDataManager.storeLastNotepad(notepadId)
        }
    }

    override fun provideNoteList(): List<Note> = notes

    override fun refreshItems() {
        this.view.showProgress(true)
        this.notesRepository.getNotes(this.notepadId)
                .observeOn(reactiveManager.schedulerAndroidMain)
                .subscribeOn(reactiveManager.schedulerIo)
                .subscribe(
                        { note: Note ->
                            if (!notes.contains(note)) notes.add(note)
                            view.notifyNoteAdded(notes.size)
                        },
                        { throwable: Throwable? ->
                            //handleError(throwable)
                        },
                        {
                            view.showProgress(false)
                        },
                        { disposable: Disposable ->
                            //view.initNotepadMenu()
                            compositeDisposable.add(disposable)
                        }
                )
    }

    companion object {
        val TAG = NotepadViewModel::javaClass.toString()
        val DEFAULT_NOTEPAD = -1
    }
}