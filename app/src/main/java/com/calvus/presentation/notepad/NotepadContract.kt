package com.calvus.presentation.notepad

import com.calvus.data.models.Note
import com.calvus.presentation.BaseContract

class NotepadContract {
    interface View : BaseContract.FragmentView {
        fun showProgress(isShowing: Boolean)
        fun changeNotepad(notepadId: Int)
        fun notifyNoteAdded(position: Int)
        fun init()
    }

    interface ViewModel : BaseContract.BaseViewModel {
        fun refreshItems()
        fun provideNoteList(): List<Note>
        fun setNotepadId(notepadId: Int)
    }

    interface InteractionListener : BaseContract.ChildInteractionListener {
        fun onNoteClicked(note: Note)
    }

}