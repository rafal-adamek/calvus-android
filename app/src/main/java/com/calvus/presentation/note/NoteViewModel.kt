package com.calvus.presentation.note

import com.calvus.data.NotesRepository
import com.calvus.data.UserDataManager
import com.calvus.domain.ReactiveManager
import com.calvus.presentation.BaseViewModel
import javax.inject.Inject

class NoteViewModel @Inject constructor(
        view: NoteContract.View,
        private val notesRepository: NotesRepository,
        private val userDataManager: UserDataManager,
        reactiveManager: ReactiveManager
) : BaseViewModel<NoteContract.View>(view, reactiveManager), NoteContract.ViewModel {

    var noteId: Int = NoteViewModel.DEFAULT_NOTE

    override fun setNote(noteId: Int) {
        this.noteId = noteId
    }

    //TODO: Default note?
    override fun init() {
        val note = notesRepository.getNote(this.noteId)
        if (note != null) {
            this.view.displayNote(note.title, note.content)
        }
    }

    companion object {
        val TAG = NoteViewModel::javaClass.toString()
        val DEFAULT_NOTE = -1
    }
}