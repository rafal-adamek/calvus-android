package com.calvus.presentation.note

import com.calvus.presentation.BaseContract

class NoteContract {
    interface View : BaseContract.FragmentView {
        fun init()
        fun displayNote(title: String, content: String)
    }

    interface ViewModel : BaseContract.BaseViewModel {
        fun init()
        fun setNote(noteId: Int)
    }

    interface InteractionListener : BaseContract.ChildInteractionListener {

    }
}