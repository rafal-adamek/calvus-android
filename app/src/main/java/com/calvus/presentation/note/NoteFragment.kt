package com.calvus.presentation.note

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.calvus.BR
import com.calvus.R
import com.calvus.databinding.FragmentNoteBinding
import com.calvus.domain.injection.NoteFragmentModule
import com.calvus.presentation.BaseFragment

class NoteFragment : BaseFragment<FragmentNoteBinding, NoteViewModel>(), NoteContract.View {
    override val layoutId: Int = R.layout.fragment_note
    override val bindingId: Int = BR.noteViewModel

    //Lifecycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.app.component.plus(NoteFragmentModule(this)).inject(this)
        this.viewModel.setNote(this.arguments?.getInt(NoteFragment.NOTE_KEY) ?: NoteViewModel.DEFAULT_NOTE)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = super.onCreateView(inflater, container, savedInstanceState)
        this.viewModel.init()
        return v
    }

    //Implementation

    override fun init() {

    }

    override fun displayNote(title: String, content: String) {
        this.root.etTitle.setText(title)
        this.root.heContent.setText(content)
    }

    //Static

    companion object {
        val TAG = NoteFragment::javaClass.toString()
        val NOTE_KEY = "NOTE_KEY"

        fun create(noteId: Int): NoteFragment {
            val fragment = NoteFragment()
            val bundle = Bundle()
            bundle.putInt(NOTE_KEY, noteId)
            fragment.arguments = bundle
            return fragment
        }
    }

}