package com.calvus.presentation

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.calvus.App
import javax.inject.Inject

abstract class BaseActivity<V : ViewDataBinding, M : BaseViewModel<*>>
    : AppCompatActivity(), BaseContract.View, BaseContract.ChildInteractionListener {

    protected lateinit var root: V

    @Inject protected lateinit var viewModel: M
    @Inject protected lateinit var appContext: Context

    @get:LayoutRes protected abstract val layoutId: Int
    protected abstract val bindingId: Int
    protected abstract val containerId: Int

    val app: App by lazy { application as App }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        root = DataBindingUtil.setContentView(this, layoutId)
        root.setVariable(bindingId, viewModel)
    }

    fun replaceFragment(
            fragment: Fragment,
            tag: String? = null,
            @IdRes containerId: Int = this.containerId,
            addToBackStack: Boolean = true,
            animationType: Int = BaseContract.ANIM_TYPE_SLIDE) {

        supportFragmentManager.beginTransaction().apply {
            if (addToBackStack) addToBackStack(null)
            else disallowAddToBackStack()

            when (animationType) {
                BaseContract.ANIM_TYPE_FADE -> setCustomAnimations(
                        android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
                BaseContract.ANIM_TYPE_SLIDE -> setCustomAnimations(
                        android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
            }

            replace(containerId, fragment, tag)
        }.commit()
    }

    fun getFragment(fragmentTag: String): Fragment? = supportFragmentManager.findFragmentByTag(fragmentTag)

    override fun onDestroy() {
        this.root.unbind()
        this.viewModel.onDestroy()
        super.onDestroy()
    }

}