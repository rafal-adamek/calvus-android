package com.calvus.presentation.login

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo

import com.calvus.R

import android.content.Intent
import android.support.design.widget.Snackbar
import android.widget.*
import com.calvus.BR
import com.calvus.databinding.ActivityLoginBinding
import com.calvus.domain.injection.LoginActivityModule
import com.calvus.presentation.BaseActivity
import com.calvus.presentation.main.MainActivity

/**
 * A login screen that offers login via email/password.
 */
class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>(), LoginContract.View {
    override val layoutId: Int = R.layout.activity_login
    override val bindingId: Int = BR.loginViewModel
    override val containerId: Int = 0

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */

    // UI references.
    private var emailView: EditText? = null
    private var passwordView: EditText? = null
    private var progressView: View? = null
    private var loginFormView: View? = null

    private val component by lazy { app.component.plus(LoginActivityModule(this)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        component.inject(this)
        setTheme(R.style.AppTheme_Dark)
        super.onCreate(savedInstanceState)
        // Set up the login form.

        emailView = root.etEmail
        passwordView = root.etPassword
        progressView = root.pbProgress
        loginFormView = root.loginForm

        passwordView?.setOnEditorActionListener(TextView.OnEditorActionListener { textView, id, keyEvent ->
            if (id == R.id.login || id == EditorInfo.IME_NULL) {
                viewModel.login(emailView?.text.toString(), passwordView?.text.toString())
                return@OnEditorActionListener true
            }
            false
        })
    }

    override fun showProgress(isShowing: Boolean) {

//        val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime)

//        loginFormView?.visibility = if (isShowing) View.GONE else View.VISIBLE
//        loginFormView?.animate()?.setDuration(shortAnimTime.toLong())?.alpha(
//                (if (isShowing) 0 else 1).toFloat())?.setListener(object : AnimatorListenerAdapter() {
//            override fun onAnimationEnd(animation: Animator) {
//                loginFormView?.visibility = if (isShowing) View.GONE else View.VISIBLE
//            }
//        })

        progressView?.visibility = if (isShowing) View.VISIBLE else View.INVISIBLE

    }

    override fun showMessage(message: String) {
        if(loginFormView != null) Snackbar.make(loginFormView!!, message, Snackbar.LENGTH_SHORT).show()
        //Toast.makeText(this, message, Snackbar.LENGTH_SHORT).show()
    }

    override fun resetErrors() {
        emailView?.error = null
        passwordView?.error = null
    }

    override fun finishLogin() {
        val mainActivity = Intent(this, MainActivity::class.java)
        startActivity(mainActivity)
        finish()
    }

    override fun showEmailError(errorRes: Int) {
        emailView?.error = getString(errorRes)
        emailView?.requestFocus()
    }

    override fun showPasswordError(errorRes: Int) {
        passwordView?.error = getString(errorRes)
        passwordView?.requestFocus()
    }
}

