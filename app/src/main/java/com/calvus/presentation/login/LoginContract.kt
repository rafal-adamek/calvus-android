package com.calvus.presentation.login

import com.calvus.presentation.BaseContract

class LoginContract {
    interface View : BaseContract.ActivityView {
        fun resetErrors()
        fun showMessage(message: String)
        fun finishLogin()
        fun showProgress(isShowing: Boolean)
        fun showEmailError(errorRes: Int)
        fun showPasswordError(errorRes: Int)
    }

    interface ViewModel : BaseContract.BaseViewModel {
        fun login(email: String, passwd: String)
        fun register(email: String, passwd: String)
    }
}