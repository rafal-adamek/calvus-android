package com.calvus.presentation.login

import com.calvus.R
import com.calvus.data.AuthDataStore
import com.calvus.domain.ReactiveManager
import com.calvus.presentation.BaseViewModel
import com.calvus.presentation.util.LogUtil
import javax.inject.Inject

class LoginViewModel @Inject constructor(
        view: LoginContract.View,
        private val authDataStore: AuthDataStore,
        reactiveManager: ReactiveManager
) : BaseViewModel<LoginContract.View>(view, reactiveManager), LoginContract.ViewModel {

    /**
     * Single = onSuccess(t: Type), onError(throwable)
     * Observable = onCompleted() [() ->], onNext(t: Type), onError(throwable)
     */
    override fun login(email: String, passwd: String) {
        if(!validateCredentials(email, passwd)) return

        view.showProgress(true)
        authDataStore.login(email, passwd)
                .observeOn(reactiveManager.schedulerAndroidMain)
                .subscribeOn(reactiveManager.schedulerNewThread)
                .subscribe({ token ->
                    authDataStore.userDataManager.storeAuthToken(token.token)
                    view.finishLogin()
                }) { throwable ->
                   handleError(throwable)
                }

    }

    override fun register(email: String, passwd: String) {
        if(!validateCredentials(email, passwd)) return

        view.showProgress(true)
        authDataStore.register(email, passwd)
                .observeOn(reactiveManager.schedulerAndroidMain)
                .subscribeOn(reactiveManager.schedulerNewThread)
                .subscribe({ _ ->
                    login(email, passwd)
                }) { throwable ->
                    handleError(throwable)
                }
    }

    fun validateCredentials(email: String, password: String): Boolean {
        var status = false
        when {
            email.isEmpty() -> view.showEmailError(R.string.error_field_required)
            password.isEmpty() -> view.showPasswordError(R.string.error_field_required)
            !isEmailValid(email) -> view.showEmailError(R.string.error_invalid_email)
            else -> status = true
        }

        return status
    }

    private fun isPasswordSecure(password: String) = true

    private fun isEmailValid(email: String) = email.contains("@")

    //TODO: Better error handling
    private fun handleError(throwable: Throwable) {
        LogUtil.d(LoginViewModel.TAG, throwable.toString())
        view.showProgress(false)
        view.showMessage(throwable.message ?: "Unknown error")
    }

    companion object {

        val TAG = LoginViewModel::javaClass.toString()
    }
}
