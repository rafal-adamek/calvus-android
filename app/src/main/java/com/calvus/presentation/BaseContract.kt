package com.calvus.presentation

abstract class BaseContract {
    companion object {
        val ANIM_TYPE_NONE = 0
        val ANIM_TYPE_FADE = 1
        val ANIM_TYPE_SLIDE = 2
    }

    interface View

    interface FragmentView : View

    interface ActivityView : View

    interface BaseViewModel {
        fun onDestroy()
    }

    interface ChildInteractionListener
}