package com.calvus.domain.injection

import com.calvus.data.NotesRepository
import com.calvus.data.UserDataManager
import com.calvus.domain.ReactiveManager
import com.calvus.presentation.notepad.NotepadContract
import com.calvus.presentation.notepad.NotepadContract.View
import com.calvus.presentation.notepad.NotepadViewModel
import dagger.Module
import dagger.Provides


@Module
class NotepadFragmentModule(internal val view: View) {

    @Provides fun provideView(): View = view

    @Provides fun provideViewModel(v: View,
                                   dm: NotesRepository,
                                   userDataManager: UserDataManager,
                                   reactiveManager: ReactiveManager): NotepadContract.ViewModel
            = NotepadViewModel(v, dm, userDataManager, reactiveManager)

}