package com.calvus.domain.injection

import android.content.Context
import com.calvus.data.NotesDataStore
import com.calvus.data.NotesRepository
import com.calvus.data.UserDataManager
import com.calvus.data.local.LocalNotesDataStore
import com.calvus.data.models.Models
import com.calvus.data.remote.CalvusApi
import com.calvus.data.remote.RemoteNotesDataStore
import dagger.Module
import dagger.Provides
import io.requery.Persistable
import io.requery.android.sqlite.DatabaseSource
import io.requery.reactivex.KotlinReactiveEntityStore
import io.requery.sql.KotlinEntityDataStore
import io.requery.sql.TableCreationMode
import javax.inject.Singleton

@Module
class NotesManagerModule {

    @Provides
    @Singleton
    fun provideNotesLocalDataStore(database: KotlinReactiveEntityStore<Persistable>, userDataManager: UserDataManager): NotesDataStore.Local
            = LocalNotesDataStore(database, userDataManager)

    @Provides
    @Singleton
    fun provideNotesRemoteDataStore(api: CalvusApi, userDataManager: UserDataManager): NotesDataStore.Remote = RemoteNotesDataStore(api, userDataManager)

    @Provides
    @Singleton
    fun provideNotesRepository(localDataStore: NotesDataStore.Local, remoteDataStore: NotesDataStore.Remote) =
            NotesRepository(localDataStore, remoteDataStore)

    @Provides
    @Singleton
    fun provideDatabase(appContext: Context): KotlinReactiveEntityStore<Persistable> {
        val source = DatabaseSource(appContext, Models.DEFAULT, 1)
        source.setTableCreationMode(TableCreationMode.DROP_CREATE)
        return KotlinReactiveEntityStore(KotlinEntityDataStore(source.configuration))
    }
}