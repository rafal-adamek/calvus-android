package com.calvus.domain.injection

import com.calvus.data.AuthDataStore
import com.calvus.data.UserDataManager
import com.calvus.data.remote.CalvusApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AuthModule {
    @Singleton @Provides fun provideAuthDataStore(calvusApi: CalvusApi, userDataManager: UserDataManager) = AuthDataStore(calvusApi, userDataManager)
}