package com.calvus.domain.injection

import com.calvus.presentation.login.LoginActivity
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(LoginActivityModule::class))
interface LoginActivityComponent {

    fun inject(activity: LoginActivity)

}