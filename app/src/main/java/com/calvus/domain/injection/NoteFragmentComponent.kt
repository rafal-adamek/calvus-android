package com.calvus.domain.injection

import com.calvus.presentation.note.NoteFragment
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(NoteFragmentModule::class))
interface NoteFragmentComponent {
    fun inject(activity: NoteFragment)
}