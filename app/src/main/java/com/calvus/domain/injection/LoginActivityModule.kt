package com.calvus.domain.injection

import com.calvus.data.AuthDataStore
import com.calvus.domain.ReactiveManager
import com.calvus.presentation.login.LoginContract.View
import com.calvus.presentation.login.LoginViewModel
import dagger.Module
import dagger.Provides

@Module
class LoginActivityModule(internal val view: View) {

    @Provides fun provideView(): View = view

    @Provides fun provideViewModel(v: View,
                                   ads: AuthDataStore,
                                   reactiveManager: ReactiveManager): LoginViewModel
            = LoginViewModel(v, ads, reactiveManager)

}