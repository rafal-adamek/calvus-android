package com.calvus.domain.injection

import com.calvus.data.UserDataManager
import com.calvus.data.NotesRepository
import com.calvus.domain.ReactiveManager
import com.calvus.presentation.main.MainContract.View
import com.calvus.presentation.main.MainViewModel
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule(internal val view: View) {

    @Provides fun provideView(): View = view

    @Provides fun provideViewModel(v: View,
                                   userDataManager: UserDataManager,
                                   notesRepository: NotesRepository,
                                   reactiveManager: ReactiveManager): MainViewModel
            = MainViewModel(v, userDataManager, notesRepository, reactiveManager)

}