package com.calvus.domain.injection

import com.calvus.presentation.notepad.NotepadContract.View
import com.calvus.presentation.notepad.NotepadFragment
import com.calvus.presentation.notepad.NotepadViewModel
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(NotepadFragmentModule::class))
interface NotepadFragmentComponent {
    fun inject(activity: NotepadFragment)
}