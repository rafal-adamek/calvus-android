package com.calvus.domain.injection

import javax.inject.Qualifier
import javax.inject.Scope

class Annotations {

    @Qualifier @Retention(AnnotationRetention.RUNTIME) annotation class AppContext

    @Qualifier @Retention(AnnotationRetention.RUNTIME) annotation class DatabaseInfo

    @Qualifier @Retention(AnnotationRetention.RUNTIME) annotation class ApiInfo

    @Qualifier @Retention(AnnotationRetention.RUNTIME) annotation class Local

    @Qualifier @Retention(AnnotationRetention.RUNTIME) annotation class Remote

    @Scope @Retention(AnnotationRetention.RUNTIME) annotation class PerApp

    @Scope @Retention(AnnotationRetention.RUNTIME) annotation class PerActivity

    @Scope @Retention(AnnotationRetention.RUNTIME) annotation class PerFragment

}
