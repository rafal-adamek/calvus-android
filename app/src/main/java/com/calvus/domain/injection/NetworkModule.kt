package com.calvus.domain.injection

import com.calvus.App
import com.calvus.data.remote.CalvusApi
import com.calvus.domain.injection.Annotations.ApiInfo
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {
    @Provides @Singleton @ApiInfo fun provideBaseUrl() = CalvusApi.Paths.BASE_URL

    @Provides fun provideGsonConverterFactory(): GsonConverterFactory = GsonConverterFactory.create()

    @Provides fun provideRxJavaAdapter(): RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()

    @Provides fun provideHttpCache(app: App): Cache {
        val cacheSize: Long = 10 * 1024 * 1024
        return Cache(app.cacheDir, cacheSize)
    }

    @Provides @Singleton fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        //gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        return gsonBuilder.create()
    }

    @Provides @Singleton fun provideOkhttpClient(cache: Cache): OkHttpClient {
        val client = OkHttpClient.Builder()
        client.cache(cache)
        return client.build()
    }

    @Provides @Singleton fun provideRetrofit(
            @ApiInfo url: String,
            gson: Gson,
            okHttpClient: OkHttpClient,
            converterFactory: GsonConverterFactory,
            rxJavaCallAdapterFactory: RxJava2CallAdapterFactory
    ): Retrofit {

        return Retrofit.Builder()
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .baseUrl(url)
                .client(okHttpClient)
                .build()
    }

    @Provides @Singleton fun provideApi(retrofit: Retrofit) = retrofit.create(CalvusApi::class.java)

}