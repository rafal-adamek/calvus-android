package com.calvus.domain.injection

import com.calvus.App
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, NetworkModule::class, NotesManagerModule::class, AuthModule::class))
interface AppComponent {
    fun inject(app: App)

    fun plus(mainActivityModule: MainActivityModule): MainActivityComponent

    fun plus(loginActivityModule: LoginActivityModule): LoginActivityComponent

    fun plus(notepadFragmentModule: NotepadFragmentModule): NotepadFragmentComponent

    fun plus(noteFragmentModule: NoteFragmentModule): NoteFragmentComponent
}