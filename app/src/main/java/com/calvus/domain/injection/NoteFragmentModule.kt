package com.calvus.domain.injection

import com.calvus.data.NotesRepository
import com.calvus.data.UserDataManager
import com.calvus.domain.ReactiveManager
import com.calvus.presentation.note.NoteContract
import com.calvus.presentation.note.NoteContract.View
import com.calvus.presentation.note.NoteViewModel
import dagger.Module
import dagger.Provides


@Module
class NoteFragmentModule(internal val view: View) {

    @Provides fun provideView(): View = view

    @Provides fun provideViewModel(v: View,
                                       dm: NotesRepository,
                                       userDataManager: UserDataManager,
                                       reactiveManager: ReactiveManager): NoteContract.ViewModel
            = NoteViewModel(v, dm, userDataManager, reactiveManager)

}