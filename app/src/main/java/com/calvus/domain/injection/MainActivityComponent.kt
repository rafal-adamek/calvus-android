package com.calvus.domain.injection

import com.calvus.presentation.main.MainActivity
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(MainActivityModule::class))
interface MainActivityComponent {

    fun inject(activity: MainActivity)

}