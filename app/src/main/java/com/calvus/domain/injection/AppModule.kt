package com.calvus.domain.injection

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.calvus.App
import com.calvus.data.UserDataManager
import com.calvus.domain.ReactiveManager
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Module(includes = arrayOf(AndroidInjectionModule::class))
class AppModule(val app: App) {

    @Provides @Singleton fun provideApp(): App = app

    //@AppContext
    @Provides @Singleton fun provideApplicationContext(): Context = app.applicationContext

    @Provides @Singleton fun provideSharedPreferences(app: App): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(app)

    @Provides @Singleton fun provideAuthTokenManager(prefs: SharedPreferences) = UserDataManager(prefs)

    @Provides fun provideReactiveManager() = ReactiveManager()
}