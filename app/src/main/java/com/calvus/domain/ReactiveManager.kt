package com.calvus.domain

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by r.adamek on 8/28/17.
 */

class ReactiveManager {

    val schedulerAndroidMain = AndroidSchedulers.mainThread()

    val schedulerIo = Schedulers.io()

    val schedulerComputation = Schedulers.computation()

    val schedulerNewThread = Schedulers.newThread()
}