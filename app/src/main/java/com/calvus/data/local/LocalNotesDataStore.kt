package com.calvus.data.local

import com.calvus.data.UserDataManager
import com.calvus.data.NotesDataStore
import com.calvus.data.models.Note
import com.calvus.data.models.NoteType
import com.calvus.data.models.Notepad
import com.calvus.data.models.NotepadType
import io.reactivex.Observable
import io.reactivex.Single
import io.requery.Persistable
import io.requery.reactivex.KotlinReactiveEntityStore
import javax.inject.Inject

class LocalNotesDataStore @Inject constructor(private val database: KotlinReactiveEntityStore<Persistable>, private val userDataManager: UserDataManager) : NotesDataStore.Local {

    //TODO: doesn't go into onNext(), only onComplete()
    override fun getNotepads(): Observable<Notepad> = database
            .select(Notepad::class)
            .orderBy(NotepadType.LAST_MODIFIED)
            .get()
            .observable()

    override fun getNotes(notepadId: Int): Observable<Note> = database
            .select(Note::class)
            .where(NoteType.NOTEPAD_ID eq notepadId)
            .orderBy(NoteType.LAST_MODIFIED)
            .get()
            .observable()

    override fun getNote(id: Int): Note? = database.select(Note::class).where(NoteType.ID eq id).get().firstOrNull()

    override fun getNotepad(id: Int): Notepad? = database.select(Notepad::class).where(NotepadType.ID eq id).get().firstOrNull()

    override fun saveNotepad(notepad: Notepad): Single<Notepad> = database.upsert(notepad)

    override fun saveNote(note: Note): Single<Note> = database.upsert(note)
}