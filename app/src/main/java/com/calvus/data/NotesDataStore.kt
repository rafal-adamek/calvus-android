package com.calvus.data

import com.calvus.data.models.Note
import com.calvus.data.models.Notepad
import io.reactivex.Observable
import io.reactivex.Single

interface NotesDataStore {

    fun getNotes(notepadId: Int): Observable<Note>
    fun getNotepads(): Observable<Notepad>
    fun saveNotepad(notepad: Notepad): Single<Notepad>
    fun saveNote(note: Note): Single<Note>


    interface Remote : NotesDataStore {
        fun addNotepad(title: String): Single<Notepad>
    }


    interface Local : NotesDataStore {
        fun getNote(id: Int): Note?
        fun getNotepad(id: Int): Notepad?
    }


}
