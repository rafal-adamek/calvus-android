package com.calvus.data

import com.calvus.data.models.Note
import com.calvus.data.models.Notepad
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

/**
 * This class manages all data and decides whether to fetch it from the server or local database.
 */

class NotesRepository @Inject constructor(
        private val localDataStore: NotesDataStore.Local,
        private val remoteDataStore: NotesDataStore.Remote) {
//
//    fun getNotepad(id: Int): Single<Notepad> =
//            localDataStore.getNotepad(id)

    fun getNotepads(): Observable<Notepad> =
            remoteDataStore.getNotepads()
                    .onErrorResumeNext { throwable: Throwable ->
                        return@onErrorResumeNext localDataStore.getNotepads()
                    }
                    .doOnNext {
                        saveNotepad(it).subscribe()
                    }

    fun getNotes(notepadId: Int): Observable<Note> =
            remoteDataStore.getNotes(notepadId)
                    .onErrorResumeNext { throwable: Throwable ->
                        return@onErrorResumeNext localDataStore.getNotes(notepadId)
                    }
                    .doOnNext {
                        saveNote(it).subscribe()
                    }

    // Remote only

    fun createNotepad(title: String) = remoteDataStore.addNotepad(title)
            .doAfterSuccess {
                saveNotepad(it).subscribe()
            }

    //Local only

    fun getNote(id: Int) = localDataStore.getNote(id)

    private fun saveNotepad(notepad: Notepad): Single<Notepad> = localDataStore.saveNotepad(notepad)

    private fun saveNote(note: Note): Single<Note> = localDataStore.saveNote(note)

}

