package com.calvus.data.models

import com.google.gson.annotations.SerializedName
import io.requery.Entity
import io.requery.Persistable

@Entity
data class User(
        @SerializedName("id") val id : Int,
        @SerializedName("login") val login : String,
        @SerializedName("email") val email: String
) : Persistable