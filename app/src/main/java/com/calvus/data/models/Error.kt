package com.calvus.data.models

import com.google.gson.annotations.SerializedName

data class Error(
        @SerializedName("error") val message: String
)