package com.calvus.data.models

import com.google.gson.annotations.SerializedName
import io.requery.Entity
import io.requery.Persistable

@Entity
data class Token(
        @SerializedName("token") val token: String
) : Persistable