package com.calvus.data.models

import android.databinding.Observable
import io.requery.Persistable

abstract class BaseObject : Persistable, Observable