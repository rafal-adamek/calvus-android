package com.calvus.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.requery.Entity
import io.requery.Key
import io.requery.Persistable

@Entity
data class Colortag (
        @SerializedName("id") @get:Key val id: String,
        @SerializedName("color") val color: String
): Persistable