package com.calvus.data.models

import com.google.gson.annotations.SerializedName
import io.requery.Entity
import io.requery.Key
import io.requery.Persistable

@Entity
data class Notepad(
        @SerializedName("id") @get:Key var id: Int,
        @SerializedName("last_modified") var lastModified: String,
        @SerializedName("name") var name: String
) : Persistable