package com.calvus.data.models

import com.google.gson.annotations.SerializedName
import io.requery.Entity
import io.requery.Key
import io.requery.Persistable

@Entity
data class Note (
        @SerializedName("id") @get:Key var id : Int,
        @SerializedName("last_modified") var lastModified : String,
        @SerializedName("title") var title : String,
        @SerializedName("content") var content : String,
        @SerializedName("colortag_id") var colortagId : Int,
        @SerializedName("notepad_id") var notepadId : Int
) : Persistable