package com.calvus.data

import android.content.SharedPreferences
import javax.inject.Inject

class UserDataManager @Inject constructor(private val prefs: SharedPreferences) {

    val token: String

    init {
        token = retrieveAuthToken()
    }

    fun storeAuthToken(token: String) {
        prefs.edit().putString(UserDataManager.KEY_TOKEN, token).apply()
    }

    //TODO: Null token?
    fun retrieveAuthToken(): String =
            prefs.getString(UserDataManager.KEY_TOKEN, UserDataManager.TOKEN_DEFAULT)

    fun storeLastNotepad(lastNotepad: Int) {
        prefs.edit().putInt(UserDataManager.KEY_LAST_NOTEPAD, lastNotepad).apply()
    }

    fun retrieveLastNotepad() = prefs.getInt(KEY_LAST_NOTEPAD, LAST_NOTEPAD_DEFAULT)

    companion object {
        val KEY_TOKEN = "KEY_TOKEN"
        val KEY_LAST_NOTEPAD = "KEY_LAST_NOTEPAD"
        val TOKEN_DEFAULT = "0"
        val LAST_NOTEPAD_DEFAULT = 0
    }
}