package com.calvus.data

import com.calvus.data.models.Token
import com.calvus.data.models.User
import com.calvus.data.remote.CalvusApi
import io.reactivex.Single
import javax.inject.Inject

class AuthDataStore @Inject constructor(private val api: CalvusApi, val userDataManager: UserDataManager){

    fun login(email: String, password: String) : Single<Token> = api.authorize(email, password)

    fun register(email: String, password: String) : Single<User> = api.registerUser(email, password)

}