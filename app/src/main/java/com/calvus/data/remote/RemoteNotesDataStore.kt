package com.calvus.data.remote

import com.calvus.data.UserDataManager
import com.calvus.data.NotesDataStore
import com.calvus.data.models.Note
import com.calvus.data.models.Notepad
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class RemoteNotesDataStore @Inject constructor(
        private val api: CalvusApi,
        private val userDataManager: UserDataManager
) : NotesDataStore.Remote {

    override fun getNotes(notepadId: Int): Observable<Note> = api
            .getNotes(userDataManager.token, notepadId)
            .flatMapIterable { x -> x }

    override fun saveNote(note: Note): Single<Note> = api
            .addNote(userDataManager.token, note.notepadId, note.content, note.title, note.colortagId)

    override fun getNotepads(): Observable<Notepad> = api
            .getNotepads(userDataManager.token, null)
            .flatMapIterable { x -> x }

    override fun saveNotepad(notepad: Notepad): Single<Notepad> = api
            .updateNotepad(userDataManager.token, notepad.id, notepad.name)

    override fun addNotepad(title: String) = api
            .addNotepad(userDataManager.token, title)
}