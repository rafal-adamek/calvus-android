package com.calvus.data.remote

import com.calvus.data.models.*
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.*

@Suppress("unused")
interface CalvusApi {

    /**
     * Get token based on given credentials
     * @param email Email of user who wants to authenticate
     * @param password Password of user who wants to authenticate
     */
    @POST(Paths.AUTH)
    fun authorize(@Query("email") email: String, @Query("password") password: String): Single<Token>

    /**
     * Register user with given name and password
     * @param email Email of the user to be created
     * @param password Password of the user to be created
     */
    @POST(Paths.USERS)
    fun registerUser(@Query("email") email: String, @Query("password") password: String): Single<User>

    /**
     * Get info about user
     * @param token Acquired token to authenticate and to identify user
     */
    @GET(Paths.USER)
    fun getUser(@Header("Authorization") token: String): Single<User>

    /**
     * Update info about user
     * @param token Acquired token to authenticate and to identify user
     * @param email New email to be used by user
     * @param password New password to be used by user
     */
    @PUT(Paths.USER)
    fun updateUser(@Header("Authorization") token: String, @Query("email") email: String, @Query("password") password: String): Single<User>

    /**
     * Delete account
     * @param token Acquired token to authenticate and to identify user
     */
    @DELETE(Paths.USER)
    fun deleteUser(@Header("Authorization") token: String): Single<User>

    /**
     * Get list of all user's notepads
     * @param token Acquired token to authenticate and to identify user
     * @param after Timestamp specifying the date the notepads we want to be returned were updated after (optional)
     */
    @GET(Paths.NOTEPADS)
    fun getNotepads(@Header("Authorization") token: String, @Query("after") after: String?): Observable<List<Notepad>>

    /**
     * Add notepad
     * @param token Acquired token to authenticate and to identify user
     * @param name Name of the notepad to be created
     */
    @POST(Paths.NOTEPADS)
    fun addNotepad(@Header("Authorization") token: String, @Query("name") name: String): Single<Notepad>

    /**
     * Get requested notepad
     * @param token Acquired token to authenticate and to identify user
     * @param notepadId Id of the requested notepad
     */
    @GET(Paths.NOTEPADS_ID)
    fun getNotepad(@Header("Authorization") token: String, @Query("notepad_id") notepadId: Int): Single<Notepad>

    /**
     * Update requested notepad
     * @param token Acquired token to authenticate and to identify user
     * @param id Id of the requested notepad
     * @param name New name of the notepad
     */
    @PUT(Paths.NOTEPADS_ID)
    fun updateNotepad(@Header("Authorization") token: String, @Path("notepad_id") id: Int, @Query("name") name: String): Single<Notepad>

    /**
     * Delete requested notepad
     * @param token Acquired token to authenticate and to identify user
     * @param id Id of the requested notepad
     */
    @DELETE(Paths.NOTEPADS_ID)
    fun removeNotepad(@Header("Authorization") token: String, @Path("notepad_id") id: Int): Single<Notepad>


    /**
     * Get list of notes for a given notepad
     * @param token Acquired token to authenticate and to identify user
     * @param notepadId Id of the notepad the note is in
     */
    @GET(Paths.NOTES_NOTEPAD)
    fun getNotes(@Header("Authorization") token: String, @Path("notepad_id") notepadId: Int): Observable<List<Note>>

    /**
     * Add a note to the given notepad
     * @param token Acquired token to authenticate and to identify user'
     * @param notepadId ID of the notepad the note is in
     * @param content Content of the note to be added (optional)
     * @param title Title of the note to be added. If not given, first 50 characters of content will be used (optional)
     * @param colortagId ID of the note colortag; if 0 - no colortag specified (0 by default) (optional)
     */
    @POST(Paths.NOTES_NOTEPAD)
    fun addNote(@Header("Authorization") token: String, @Path("notepad_id") notepadId: Int,
                @Query("content") content: String?, @Query("title") title: String?, @Query("colortag_id") colortagId: Int?): Single<Note>

    /**
     * Get requested note
     * @param token Acquired token to authenticate and to identify user
     * @param notepadId ID of the notepad the note is in
     * @param noteId ID of the note
     */
    @GET(Paths.NOTES_NOTEPAD_ID)
    fun getNote(@Header("Authorization") token: String, @Path("notepad_id") notepadId: Int, @Path("noteId") noteId: Int): Single<Note>

    /**
     * Update requested note
     * @param token Acquired token to authenticate
     * @param notepadId ID of the notepad the note is in
     * @param noteId ID of the note
     * @param content Content of the note to be added (optional)
     * @param title Title of the note to be added. If not given, first 50 characters of content will be used (optional)
     * @param newNotepadId ID of the notepad to move the note to (optional)
     * @param colortagId ID of the new note colortag (optional)
     * @return observable of Note
     */
    @PUT(Paths.NOTES_NOTEPAD_ID)
    fun updateNote(@Header("Authorization") token: String, @Path("notepad_id") notepadId: Int, @Path("noteId") noteId: Int,
                   @Query("content") content: String?, @Query("title") title: String?,
                   @Query("new_notepad_id") newNotepadId: Int?, @Query("colortag_id") colortagId: Int?): Single<Note>

    /**
     * Delete requested note
     * @param token Acquired token to authenticate
     * @param notepadId ID of the notepad the note is in
     * @param noteId ID of the note
     * @return Single Note
     */
    @DELETE(Paths.NOTES_NOTEPAD_ID)
    fun removeNote(@Header("Authorization") token: String, @Path("notepad_id") notepadId: Int, @Path("noteId") noteId: Int): Single<Note>

    /**
     * Get colortags
     * @param token Acquired token to authenticate
     */
    @GET(Paths.COLORTAGS)
    fun getColortags(@Header("Authorization") token: String): Observable<Colortag>


    object Paths {
        internal const val BASE_URL = "https://calvus.mbiel.pl/api/v1/"
        internal const val AUTH = "auth"
        internal const val USER = "user"
        internal const val USERS = "users"
        internal const val NOTEPADS = "notepads"
        internal const val NOTEPADS_ID = "notepads/{notepad_id}"
        internal const val NOTES_NOTEPAD = "notes/{notepad_id}"
        internal const val NOTES_NOTEPAD_ID = "notes/{notepad_id}/{note_id}"
        internal const val COLORTAGS = "colortags"
    }
}