package com.calvus.presentation.login

import com.calvus.R
import com.calvus.data.AuthDataStore
import com.calvus.data.UserDataManager
import com.calvus.data.models.Token
import com.calvus.presentation.ViewModelTest
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito

class LoginViewModelTest: ViewModelTest() {

    private lateinit var viewModel: LoginViewModel

    @Mock lateinit var loginViewMock: LoginContract.View
    @Mock lateinit var authDataStoreMock: AuthDataStore
    @Mock lateinit var userDataManagerMock: UserDataManager

    @Before
    override fun setup() {
        super.setup()
        viewModel = LoginViewModel(loginViewMock, authDataStoreMock, reactiveManagerMock)
    }

    @Test
    fun shouldSaveTokenOnLoginSuccess() {
        Mockito.`when`(authDataStoreMock.login(anyString(), anyString())).thenReturn(Single.just(Token("token")))
        Mockito.`when`(authDataStoreMock.userDataManager).thenReturn(userDataManagerMock)
        viewModel.login("test@test.pl", "asdasd")
        testScheduler.triggerActions()
        Mockito.verify(userDataManagerMock).storeAuthToken(ArgumentMatchers.anyString())
    }

    @Test
    fun shouldShowMessageOnLoginFailure() {
        Mockito.`when`(authDataStoreMock.login(anyString(), anyString())).thenReturn(Single.error { Throwable() })
        viewModel.login("test@test.pl", "asdasd")
        testScheduler.triggerActions()
        Mockito.verify(loginViewMock).showMessage(anyString())
    }

    @Test
    fun shouldShowEmailInvalidError() {
        viewModel.validateCredentials("test", "test")
        Mockito.verify(loginViewMock).showEmailError(ArgumentMatchers.eq(R.string.error_invalid_email))
    }


    @Test
    fun shouldShowEmailEmptyError() {
        viewModel.validateCredentials("", "test")
        Mockito.verify(loginViewMock).showEmailError(ArgumentMatchers.eq(R.string.error_field_required))
    }

    @Test
    fun shouldShowPasswordEmptyError() {
        viewModel.validateCredentials("test@test.pl", "")
        Mockito.verify(loginViewMock).showPasswordError(ArgumentMatchers.eq(R.string.error_field_required))
    }
}