package com.calvus.presentation.notepad

import com.calvus.data.NotesRepository
import com.calvus.data.UserDataManager
import com.calvus.presentation.ViewModelTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class NotepadViewModelTest : ViewModelTest() {

    lateinit var notepadViewModel: NotepadViewModel

    @Mock lateinit var notepadViewMock: NotepadContract.View
    @Mock lateinit var notesRepositoryMock: NotesRepository
    @Mock lateinit var userDataManagerMock: UserDataManager

    @Before
    override fun setup() {
        super.setup()
        notepadViewModel = NotepadViewModel(
                notepadViewMock, notesRepositoryMock, userDataManagerMock, reactiveManagerMock)
    }

    @Test
    fun test() {
    }

}