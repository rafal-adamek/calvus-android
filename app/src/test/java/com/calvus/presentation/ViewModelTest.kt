package com.calvus.presentation

import com.calvus.domain.ReactiveManager
import com.calvus.presentation.login.LoginViewModel
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by r.adamek on 8/28/17.
 */

@RunWith(MockitoJUnitRunner::class)
abstract class ViewModelTest {

    @Mock lateinit var reactiveManagerMock: ReactiveManager

    protected val testScheduler = TestScheduler()

    @Before
    open fun setup() {
        MockitoAnnotations.initMocks(this)
        Mockito.`when`(reactiveManagerMock.schedulerAndroidMain).thenReturn(testScheduler)
//        Mockito.`when`(reactiveManagerMock.schedulerComputation).thenReturn(testScheduler)
//        Mockito.`when`(reactiveManagerMock.schedulerIo).thenReturn(testScheduler)
        Mockito.`when`(reactiveManagerMock.schedulerNewThread).thenReturn(testScheduler)
    }
}