package com.calvus.presentation.main

import com.calvus.data.UserDataManager
import com.calvus.data.NotesRepository
import com.calvus.data.models.Notepad
import com.calvus.data.models.Token
import com.calvus.presentation.ViewModelTest
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify

class MainViewModelTest: ViewModelTest() {

    private lateinit var viewModel: MainViewModel

    @Mock private lateinit var userDataManagerMock: UserDataManager

    @Mock private lateinit var notesRepositoryMock: NotesRepository

    @Mock private lateinit var viewMock: MainContract.View

    @Before
    override fun setup() {
        super.setup()
        viewModel = MainViewModel(viewMock, userDataManagerMock, notesRepositoryMock, reactiveManagerMock)
    }

    @Test
    fun shouldOpenLoginScreenOnInvalidToken() {
        Mockito.`when`(userDataManagerMock.retrieveAuthToken()).thenReturn(UserDataManager.TOKEN_DEFAULT)
        viewModel.validateToken()
        verify(viewMock).openLoginScreen()
    }


    @Test
    fun shouldInitViewOnValidToken() {
        Mockito.`when`(userDataManagerMock.retrieveAuthToken()).thenReturn("token")
        viewModel.validateToken()
        verify(viewMock).initMain()
    }

    @Test
    fun shouldAddNotepadOnSuccessfulFetch() {
        val notepad = Notepad(1, "", "")
        Mockito.`when`(notesRepositoryMock.getNotepads()).thenReturn(Observable.just(notepad))
        viewModel.fetchNotepads()
        testScheduler.triggerActions()
        verify(viewMock).addToNotepadMenu(notepad)
    }

    @Test
    fun shouldShowProgressOnFetchNotepadsInvoked() {
        Mockito.`when`(notesRepositoryMock.getNotepads()).thenReturn(Observable.just(Notepad(1, "", "")))
        viewModel.fetchNotepads()
        verify(viewMock).showProgress(true)
    }

    @Test
    fun shouldInitNotepadMenuOnFetchNotepadsInvoked() {
        Mockito.`when`(notesRepositoryMock.getNotepads()).thenReturn(Observable.just(Notepad(1, "", "")))
        viewModel.fetchNotepads()
        testScheduler.triggerActions()
        verify(viewMock).initNotepadMenu()
    }

    @Test
    fun shouldStopProgressOnFetchNotepadsComplete() {
        Mockito.`when`(notesRepositoryMock.getNotepads()).thenReturn(Observable.just(Notepad(1, "", "")))
        viewModel.fetchNotepads()
        testScheduler.triggerActions()
        verify(viewMock).showProgress(false)
    }
}